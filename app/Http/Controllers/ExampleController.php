<?php

namespace App\Http\Controllers;


use Laravel\Lumen\Http\Request;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function firstFunction(Request $request){
        return response()->json(['status' => 'HTTP_OK']);
    }

    //
}
