<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $result;
    private $userModel;

    public function __construct(\stdClass $result, User $user)
    {
        $this->result = $result;
        $this->userModel = $user;
    }

    public function store(Request $request)
    {
        try {
            $responseCode = Response::HTTP_OK;
            $validator = Validator::make($request->all(), [

            ]);
            if ($validator->fails()) {
                $responseCode = Response::HTTP_BAD_REQUEST;
                $this->result->errors = $validator->errors()->all();
            } else {
                //code
            }
            return response()->json($this->result, $responseCode);
        } catch (\Exception $e) {
            Log::info($e);
            $customMessage = $e->getMessage();
            $customCode = $e->getCode();
            $customCode = (isset($customCode) && isset(Response::$statusTexts[$customCode])) ? $customCode : Response::HTTP_INTERNAL_SERVER_ERROR;
            $this->result->message = (isset($customMessage) && !empty($customMessage && isset(Response::$statusTexts[$customCode]) && $customCode != Response::HTTP_INTERNAL_SERVER_ERROR)) ? $customMessage : "Internal Server Error";
            return response()->json($this->result, $customCode);
        }

    }
}
